import pytest
from src.json_webpage_parser import JsonWebpage

if __name__ == '__main__':
    webpage = JsonWebpage()
    oecd_dict = webpage.get_oecd_json()
    oecd_dict.calculate_unemployment_rate()
    pytest.main()
