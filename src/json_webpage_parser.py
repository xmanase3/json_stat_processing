from urllib.request import urlopen
from urllib.error import URLError
from src.oecd_file import OECDJson
import json


class JsonWebpage:
    def __init__(self,
                 url: str = "https://json-stat.org/samples/oecd.json") -> None:
        self.url = url

    def get_oecd_json(self) -> OECDJson:
        try:
            with urlopen(self.url) as response:
                return OECDJson(json.loads(response.read().decode("utf-8")))
        except URLError:
            print("URL cannot be reached")
            raise
        except json.JSONDecodeError:
            print("Target is not JSON file")
            raise
