from math import inf
from typing import Dict, List, Set, Tuple, Any, Optional


class OECDJson:
    def __init__(self, data: Dict) -> None:
        self.data = data

    def get_values(self) -> List[int]:
        return self._get_path('value')

    def get_years_legend(self) -> Dict[str, int]:
        return self._get_path('dimension', 'year', 'category', 'index')

    def get_areas_legend(self) -> Dict[str, int]:
        return self._get_path('dimension', 'area', 'category', 'index')

    def get_areas_names(self) -> Dict[str, str]:
        return self._get_path('dimension', 'area', 'category', 'label')

    def _get_path(self, *path: str) -> Any:
        curr = self.data
        for node in path:
            if node not in curr:
                print("Input data are not in OECD format")
                raise ValueError("Invalid key")
            curr = curr[node]
        return curr

    def calculate_unemployment_rate(
            self, years: Optional[Set[str]] = None) -> Tuple[List[str], List[str]]:
        best_three_sorted: List[Tuple[float, str]] = [
            (inf, ''), (inf, ''), (inf, '')]
        worst_three_sorted: List[Tuple[float, str]] = [
            (-inf, ''), (-inf, ''), (-inf, '')]
        for area_identifier, area_position in self.get_areas_legend().items():
            if area_identifier == 'OECD' or area_identifier == 'EU15':
                continue
            area_average_unemployment = self.area_average_rate(
                years, area_position)
            belong_to_worst_check(
                area_identifier,
                area_average_unemployment,
                worst_three_sorted)
            belong_to_best_check(
                area_identifier,
                area_average_unemployment,
                best_three_sorted)

        print("Countries with highest unemployment rate:")
        self._print_unemployment_rates(worst_three_sorted)
        print("Countries with lowest unemployment rate:")
        self._print_unemployment_rates(best_three_sorted)

        return [record[1] for record in worst_three_sorted], [record[1]
                                                              for record in best_three_sorted]

    def area_average_rate(self,
                          years: Optional[Set[str]],
                          area_position: int) -> float:
        values: List[int] = self.get_values()
        years_legend: Dict[str, int] = self.get_years_legend()
        years_total: int = len(years_legend)
        if years is None:
            years = years_legend.keys()
        if len(years) == 0:
            print('Enter at least one year')
            raise ValueError('Years set empty')
        if area_position < 0:
            print('Index has to be positive')
            raise ValueError('Invalid index')

        sum_of_rates: float = 0.0
        for year in years:
            if year not in years_legend:
                print('Invalid year')
                raise ValueError('Year do not have any index')

            record_position = area_position * years_total + years_legend[year]
            if record_position < 0 or record_position >= len(values):
                print("Input data are not in JSON-stat format")
                raise ValueError("Invalid value index")

            sum_of_rates += values[record_position]
        return sum_of_rates / len(years)

    def _print_unemployment_rates(
            self, areas: List[Tuple[float, str]]) -> None:
        area_names = self.get_areas_names()
        for unemployment_rate, area_identifier in areas:
            print(
                "{} with average unemployment rate {}".format(
                    area_names[area_identifier], round(
                        unemployment_rate, 2)))
        print()


def belong_to_worst_check(area_name: str, curr_average_unemployment: float,
                          worst_three_sorted: List[Tuple[float, str]]) -> None:
    if curr_average_unemployment > worst_three_sorted[2][0]:
        if curr_average_unemployment > worst_three_sorted[1][0]:
            if curr_average_unemployment > worst_three_sorted[0][0]:
                worst_three_sorted[2] = worst_three_sorted[1]
                worst_three_sorted[1] = worst_three_sorted[0]
                worst_three_sorted[0] = (curr_average_unemployment, area_name)
            else:
                worst_three_sorted[2] = worst_three_sorted[1]
                worst_three_sorted[1] = (curr_average_unemployment, area_name)
        else:
            worst_three_sorted[2] = (curr_average_unemployment, area_name)


def belong_to_best_check(area_name: str, curr_average_unemployment: float,
                         best_three_sorted: List[Tuple[float, str]]) -> None:
    if curr_average_unemployment < best_three_sorted[2][0]:
        if curr_average_unemployment < best_three_sorted[1][0]:
            if curr_average_unemployment < best_three_sorted[0][0]:
                best_three_sorted[2] = best_three_sorted[1]
                best_three_sorted[1] = best_three_sorted[0]
                best_three_sorted[0] = (curr_average_unemployment, area_name)
            else:
                best_three_sorted[2] = best_three_sorted[1]
                best_three_sorted[1] = (curr_average_unemployment, area_name)
        else:
            best_three_sorted[2] = (curr_average_unemployment, area_name)
