from urllib.error import URLError
from src.json_webpage_parser import JsonWebpage
import pytest
import json


def test_json_webpage():
    invalid_url = JsonWebpage("https://json-stat.org/samples/invalid")
    with pytest.raises(URLError):
        invalid_url.get_oecd_json()

    invalid_website_format = JsonWebpage(
        "https://en.wikipedia.org/wiki/42_(number)")
    with pytest.raises(json.JSONDecodeError):
        invalid_website_format.get_oecd_json()
