from src.oecd_file import belong_to_best_check, belong_to_worst_check
from src.json_webpage_parser import JsonWebpage
from math import isclose
import pytest


def test_belong_to():
    worst_list = [(50.99, 'Zimbabwe'), (12.56, 'Russia'), (2.2, 'Nederland')]
    best_list = [(2.2, 'Nederland'), (12.56, 'Russia'), (50.99, 'Zimbabwe')]

    belong_to_best_check('Uganda', 60.44, worst_list)
    belong_to_worst_check('Neverland', 0.0, best_list)
    assert best_list == [
        (2.2, 'Nederland'), (12.56, 'Russia'), (50.99, 'Zimbabwe')]
    assert worst_list == [
        (50.99, 'Zimbabwe'), (12.56, 'Russia'), (2.2, 'Nederland')]

    belong_to_best_check('Neverland', 0.0, best_list)
    belong_to_worst_check('Uganda', 60.44, worst_list)
    assert best_list == [
        (0.0, 'Neverland'), (2.2, 'Nederland'), (12.56, 'Russia')]
    assert worst_list == [
        (60.44, 'Uganda'), (50.99, 'Zimbabwe'), (12.56, 'Russia')]


def test_area_average_rate():
    web = JsonWebpage()
    oecd_json = web.get_oecd_json()
    with pytest.raises(ValueError):
        oecd_json.area_average_rate({'1620'}, 0)
    with pytest.raises(ValueError):
        oecd_json.area_average_rate({'2004', '1620', '2006'}, 0)
    with pytest.raises(ValueError):
        oecd_json.area_average_rate({'2003'}, -1)
    with pytest.raises(ValueError):
        oecd_json.area_average_rate({'2014'}, 36)
    with pytest.raises(ValueError):
        oecd_json.area_average_rate(set(), 0)

    assert isclose(oecd_json.area_average_rate({'2003'}, 0), 5.943826289)
    assert isclose(oecd_json.area_average_rate({'2014'}, 35), 8.004598637)
    assert '{0:.2f}'.format(oecd_json.area_average_rate(
        {'2003', '2004'}, 0)) == '5.67'
    assert '{0:.2f}'.format(oecd_json.area_average_rate(None, 26)) == '13.85'


def test_calculate_unemployment_rate():
    web = JsonWebpage()
    oecd_json = web.get_oecd_json()
    assert oecd_json.calculate_unemployment_rate() \
        == (['ES', 'GR', 'SK'], ['NO', 'KR', 'CH'])
    assert oecd_json.calculate_unemployment_rate({'2003'}) \
        == (['PL', 'SK', 'IL'], ['MX', 'LU', 'IS'])
    assert oecd_json.calculate_unemployment_rate({'2003', '2005', '2007', '2009', '2011', '2013'}) \
        == (['ES', 'SK', 'GR'], ['NO', 'KR', 'CH'])
